# GUI на Python с помощью tkinter
import tkinter as tk
from tkinter import messagebox

# Создание нового окна
root = tk.Tk()

# Получение размеров экрана
screen_h = root.winfo_screenheight()
screen_w = root.winfo_screenwidth()

# Здесь мы создали переменные, где будем указывать размеры нашего окна
width = 400
height = 300

# Вычисляем отступы слева и сверху, чтобы наше окно было посередине экрана
offset_x = screen_w / 2 - width / 2
offset_y = screen_h / 2 - height / 2

# Заголовок окна
root.title("Codify App")

# А здесь задаем размеры и отступы окна
root.geometry("{}x{}+{}+{}".format(width, height, int(offset_x), int(offset_y)))

# Сам формат такой:
# root.geometry("300x400+100+100")

# Этот виджет StringVar нужен для того чтобы менять текст в Label
top_label_var = tk.StringVar(root, value="Hello, World!")

# Это виджет Label, отображает просто текст. Этот виджет берет текст из top_label_var
top_label = tk.Label(root, textvariable=top_label_var, bg="#FF0000", fg="white",
                     font=("Times New Roman", 12))

# Метод pack() размещает виджет на экране
top_label.pack(fill=tk.BOTH, expand=1, padx=20, pady=10)

# А у этого Label текст всегда один.
label2 = tk.Label(root, text="Алло, привет", bg="blue", fg="white", font=("Arial", 20))
label2.pack(ipadx=20, ipady=20)

# Это виджет для ввода текста
input1 = tk.Text(root, width=30, height=1, bg="#EEE")
input1.pack(fill=tk.X)


def on_calc_click():
    text = input1.get(1.0, tk.END)

    top_label_var.set("Результат: " + text)


def on_message_click():
    messagebox.showinfo("Сообщение", "Кнопка 'Вычислить' была нажата")


# Виджет - кнопка, когда нажимаем на кнопку вызывается функция
# указанная через параметр command, в данном случае on_calc_click
calc_button = tk.Button(root, text="Вычислить", command=on_calc_click)
calc_button.pack(fill=tk.BOTH, expand=1, padx=20, pady=10)


message_button = tk.Button(root, text="Показать сообщение", command=on_message_click)
message_button.pack(fill=tk.BOTH, expand=1, padx=20, pady=10)

root.mainloop()

# В метода pack есть много разных параметров:
# fill - заполнить окно. можно указать:
#       tk.X - чтобы виджет заполнил по горизонтали
#       tk.Y - чтобы виджет заполнил по вертикали
#       tk.BOTH - чтобы виджет заполнил по вертикали и по горизонтали
# expand - надо указывать 1, если вы хотите заполнить все оставшееся пространство.
# padx, pady - внешние отступы от виджета по горизонтали и по вертикали
# ipadx, ipady - внутренние отступы от виджета по горизонтали и по вертикали