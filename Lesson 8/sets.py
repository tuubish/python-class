set1 = set()
set2 = {2, 3, 4, 5, 6}

print(set2)
# print(set2[3]) множества не имеют индекса

for item in set2:
    print(item)

print("Len:", len(set2))

# Добавление элемента
name = {"Alisher", "Nail", "Aidana"}
name.add("Sabina")

print(name)
