# empdict = dict()
#
# dicr1 = {
#     "a": 1,
#     "b": 2,
#     "codify": 3
# }
#

aidana = {
    "age": 22,
    "height": 168,
    "weight": 40,
    "name": "Aidana",
    "last_name": "Borisova"
}

name = aidana["name"] # Присвоивание к переменной

aidana["year_of_birth"] = 1997 # Добавление словаря

print(name, end=" ")
print(aidana["last_name"])

aidana["last_name"] = "Musk" # изменение значения

print(aidana)

del aidana["weight"] # Удаление ключа со значением
print(aidana)

print(len(aidana))

for dicts in aidana.keys():
    print(dicts, ":", aidana[dicts])

print()

for key, value in aidana.items():
    print(key, ":", value)

marks = dict() # marks = {}

marks["Aidana"] = 98
marks["Kubat"] = 80
marks["nail"] = 45

