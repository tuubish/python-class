# наибольший общий делитель
# 1. Большее число делим на меньшее.
# 2. Если делится без остатка, то меньшее число и есть НОД (следует выйти из цикла).
# 3. Если есть остаток, то большее число заменяем на остаток от деления.
# 4. Переходим к пункту 1.
# GCD = Greatest Common Divider


def gcd(number1: int, number2: int) -> int:
    while True:
        big = max(number1, number2)
        small = min(number1, number2)

        reminder = big % small
        if reminder == 0:
            return small
        else:
            number1 = reminder
            number2 = small


print(gcd(33, 27))