from main import plus

result_10_plus_5 = plus(10, 5)
if result_10_plus_5 == 15:
    print("Test 10+5 passed")
else:
    print("Test 10+5 failed")

result2 = plus(1234, 789)
if result2 == 2023:
    print("Test 1234+789 passed")
else:
    print("Test 1234+789 failed")