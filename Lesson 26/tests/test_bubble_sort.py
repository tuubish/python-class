# Task 1:
# Написать тесты для функции игииду_ыщке
# по TDD
# т.е. 1. сначала написать тесты (4 шт), потом запустить тесты и убедиться что все тесты провалены
# 2. исправить функцию
# 3. Снвоо запустить тесты и убедиться, что все они прошли успешно

from main import bubble_sort

def sortTest(expect, result):
    if expect == result:
        print("Test passed")
    else:
        raise AssertionError("Test failed: Expected {}, returned {}". format(expect, result))

list1 = [2, 4, 1]
bubble_sort(list1)
sortTest([1, 2, 4], list1)

list2 = [74, 69, 2, 7, 83, 28, 6, 4]
bubble_sort(list2)
sortTest([2, 4, 6, 7, 28, 69, 74, 83], list2)