class Fridge:
    def __init__(self):
        self.is_active = False
        self.items = []

    def turn_on(self):
        self.is_active = True

    def add_item(self, new_item):
        self.items.append(new_item)


beko = Fridge()
beko.turn_on()
beko.add_item("Apple")
beko.add_item("Airan")

print(beko.items)