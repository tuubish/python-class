# Создать функцию draw_bar_chart() в файле barchart.py
# которая рисует диаграмму столбиками
# используя функцию draw_rect()
# width - ширина столбиков
# values - список высот


import turtle
from barchart import draw_bar_chart
from shapes import Rect, Circle
from drawer import Drawer

turtle.reset()

rect1 = Rect()
rect1.width = 10
rect1.height = 10
rect1.color = "red"

rect2 = Rect()
rect2.width = 12
rect2.height = 12
rect2.color = "blue"

rect3 = Rect()
rect3.width = 20
rect3.height = 34
rect3.color = "cyan"

circle1 = Circle()
circle1.radius = 15
circle1.color = "yellow"

rect4 = Rect()
rect4.width = 12
rect4.height = 130
rect4.color = "pink"

shapes = [rect1, rect2, rect3, circle1, rect4]

drawer = Drawer()

drawer.draw_rect(turtle, rect1)

turtle.mainloop()
