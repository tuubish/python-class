class Drawer:
    def __init__(self, turtle):
        self.turtle = turtle

    def prepare(self):
        self.turtle.reset()

    def finish(self):
        self.turtle.mainloop()

    def draw_shape(self, shape):
        shape.draw(self.turtle)