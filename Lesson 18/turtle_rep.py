import turtle

turtle.reset()

turtle.fillcolor("red")

turtle.begin_fill()

turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(100)

turtle.end_fill()

turtle.penup()
turtle.forward(100)
turtle.pendown()

turtle.circle(40)

turtle.mainloop()