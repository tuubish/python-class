print("Hello everyone!")

input_file = open("files/test.txt", "r", encoding="utf-8")

#contents = input_file.read() # Считывает файл
read_lines = input_file.readlines()

for line in read_lines:
    print(line)

#print(contents)


print(read_lines)


input_file.close()

input_file = open("files/test.txt", "r", encoding="utf-8")

#read_line = input_file.readline() # Считывает строки по очереди
# print(read_line)
# print(input_file.readline())
# print(input_file.readline())

input_file.close()


#strip() - Убирает все пробел с двух сторон строки
input_file = open("files/test.txt", "r", encoding="utf-8")
read_line = input_file.readline() # Считывает строки по очереди
while True:
    line = input_file.readline()
    if line == "":
        break
    print(line, end="")

input_file.close()