from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import requests

TOKEN = "1313817199:AAFO43knZcJNIPDJ7GXXunOm2l50tBcp95Q"

print("Bot successfully started. Press Ctrl+F2 to exit.")

def get_rate(source, result):
    url = "https://api.exchangeratesapi.io/latest?base=" + source
    response = requests.get(url)

    r = response.json()
    ruble_rate = r["rates"][result]
    return ruble_rate


def on_start(update, context):
    chat = update.effective_chat
    context.bot.send_message(chat_id=chat.id, text="Hi! I'm started")

def on_convert(update, context):
    chat = update.effective_chat
    context.bot.send_message(chat_id=chat.id, text="Chooce currency to convert")

def on_message(update, context):
    chat = update.effective_chat
    text = update.message.text
    # Формат сообщения: USD 12 to RUB

    try:
        parts = text.strip().split()
        current_currency = parts[0]
        result_currency = parts[3]
        number = float(parts[1])
        rate = get_rate(current_currency, result_currency)
        result = number * rate
        message = "%s %.2f = %s %.2f" % (current_currency, number, result_currency, result)
        context.bot.send_message(chat_id=chat.id, text=message)
    except:
        context.bot.send_message(chat_id=chat.id, text="Enter number to convert")

updater = Updater(TOKEN, use_context=True)
dispatcher = updater.dispatcher

dispatcher.add_handler(CommandHandler("start", on_start))
dispatcher.add_handler(CommandHandler("convert", on_convert))
dispatcher.add_handler(MessageHandler(Filters.all, on_message))

updater.start_polling()

updater.idle()