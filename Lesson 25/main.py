# Алгоритмы

# Алгоритм поиска элемента в списке

# Входные данные: список с элементами и искомый элемент

def fined_element(array: list, element) -> int:
    for i in range(len(array)):
        current = array[i]
        if current == element:
            return i
    return  -1


array = [2, 4, 3, 55, 7, 23, 88]
print(fined_element(array, 3))
print(fined_element(array, 88))
print(fined_element())