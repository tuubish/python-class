import re

text = "Меня зовут Армстронг. Я Король Англии."

result = re.sub(r"Я Король (\w*)", "Я Король Земли", text)
print(result)

text2 = "1003 метр. 123123 км 333"

result2 = re.sub(r"\d+", "***", text2)
print(result2)

text2 = "1003 метр. 123123 км 333"

result3 = re.split(r"\d+", text2)
print(result3)

