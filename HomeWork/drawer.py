from rectangle import draw_rect
from shapes import *

class Drawer:
    def __init__(self):
        self.default_color = "red"

    def draw_bar_chart(self, turtle, shapes):
        for shape in shapes:
            if isinstance(shape, Rect):
                self.draw_rect(turtle, shape)
            elif isinstance(shape, Circle):
                self.draw_circle(turtle, shape)

    def draw_rect(self, turtle, rect):
        height = rect.height
        if rect.color is None:
            color = self.default_color
        else:
            color = rect.color
        width = rect.width
        draw_rect(turtle, width, height, color)
        turtle.forward(width)

    def draw_circle(self, turtle, circle):
        radius = circle.radius
        color = circle.color
        turtle.forward(radius)
        turtle.fillcolor(color)
        turtle.begin_fill()
        turtle.circle(radius)
        turtle.end_fill()
        turtle.forward(radius)