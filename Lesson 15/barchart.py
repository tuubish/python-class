from rectangle import draw_rect
from shapes import *


def draw_bar_chart(turtle, shapes):
    for shape in shapes:
        if isinstance(shape, Rect):
            height = shape.height
            color = shape.color
            width = shape.width
            draw_rect(turtle, width, height, color)
            turtle.forward(width)
        elif isinstance(shape, Circle):
            radius = shape.radius
            color = shape.color
            turtle.forward(radius)
            turtle.fillcolor(color)
            turtle.begin_fill()
            turtle.circle(radius)
            turtle.end_fill()
            turtle.forward(radius)