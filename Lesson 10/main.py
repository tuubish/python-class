# Datetime
from datetime import datetime, date, time, timedelta, timezone

now = datetime.today()

print(now)

date1 = datetime(2012, 12, 1)
print(date1)

datetime1 = datetime(2012, 12, 1, 7, 45, 10)
print(datetime1)

time1 = time(7, 10, 12)
print(time1)

print("Date:", now.date())
print("Time:", now.time())

