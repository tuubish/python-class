# Main file of Lesson 14 - Черепашка
import turtle

turtle.reset()
turtle.tracer()

turtle.forward(100)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(100)
turtle.left(180)
turtle.forward(100)
turtle.right(45)
turtle.forward(74)
turtle.right(90)
turtle.forward(74)

turtle.up()

turtle.left(45)
turtle.forward(100)
turtle.down()

turtle.left(45)
turtle.forward(100)
turtle.right(90)
turtle.color("blue")
turtle.forward(100)
turtle.right(90)
turtle.color("red")
turtle.forward(100)
turtle.right(90)
turtle.color("yellow")
turtle.forward(100)
turtle.right(180)
turtle.color("brown")
turtle.forward(100)
turtle.right(45)
turtle.forward(100)



turtle.tracer(0)






turtle.mainloop()