from email import is_correct_email
from tests.assertions import assertEqual, assertFalse, assertTrue

assertTrue(is_correct_email("slkdskldj@gmail.com"))
assertTrue(is_correct_email("kubat.usubaliev@gmail.com"))
assertFalse(is_correct_email("kubat.usubaliev@@gmail.com"))
assertFalse(is_correct_email("@gmail.com"))
assertFalse(is_correct_email("kubatgmail.com"))
