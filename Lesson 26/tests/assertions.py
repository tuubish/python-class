def assertEqual(expected, actual):
    if expected == actual:
        print("Test passed")
    else:
        raise AssertionError("Test failed: Expected {}, returned {}". format(expected, actual))

def assertTrue(actual):
    if actual == True:
        print("Test passed")
    else:
        raise AssertionError("Test failed: {} - is not True".format(actual))

def assertFalse(actual):
    if actual == False:
        print("Test passed")
    else:
        raise AssertionError("Test failed: {} - is not False".format(actual))