# Создать функцию draw_colored_bar_chart() которая рисует
# диаграмму столбиками
# width - ширина столбиков
# values - список высот
# min - нижняя граница (все значения меньше min красным цветом)
# max - верхняя граница (все значения больше max зеленым цветом)
# все остальное синим цветом

import turtle
from color_barchart import draw_colored_bar_chart

turtle.reset()

draw_colored_bar_chart(turtle, [180, 160, 170, 190, 214], 12, 170, 200)

turtle.mainloop()