# Чтение файла

file1 = open("example.txt", "r")

line = file1.read()
print(line)

# Создание и запись в файл

file2 = open("script.py", "w")

file2.write("from datetime import datetime\n\n")
file2.write("print(datetime.now())\n")

file2.close()

# Дополнить файл

file3 = open("newfile.qwerty", "a")
file3.write("New lines\n")
file3.write("New lines 2\n")
file3.write("New lines 3\n")
file3.close()
