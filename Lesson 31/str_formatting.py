apple = "Apple"
price = 23.4

new_str = "Product: " + apple + " price: " + str(price)
new_str2 = "Product: {} price: {}".format(apple, price)
new_str3 = f"Product: {apple} price: {price}"

print(new_str)
print(new_str2)
print(new_str3)