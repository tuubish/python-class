from datetime import timedelta, datetime
# как брибавить дату и время
one_day = timedelta(days=2)
print(one_day)

date1 = datetime(2020, 3, 1, 9, 0)
print(date1)

date2 = date1 + timedelta(hours=1)
print(date2)

date3 = date1 + timedelta(days=40)
print(date3)

date4 = date1 - timedelta(hours=9)
print(date4)

date5 = date1 - timedelta(days=10)
print(date5)