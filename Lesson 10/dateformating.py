from datetime import datetime

now = datetime.now()

print(now.strftime("%a %A"))
print(now.strftime("%d %b"))
print(now.strftime("%a %b %d %H:%M"))
print(now.strftime("%I:%M %p"))