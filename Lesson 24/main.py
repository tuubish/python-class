# Регулярные выражения - (купуч)
import re

match = re.search(r"Мне (\d+) (лет)", " Мне 123467 111 лет")
print(match.group(0))

email_regex = r"[\w.]+@\w+\.\w+"
def is_correct_email(email):
    match = re.match(email_regex, email)
    if match != None:
        return True
    else:
        return False


print(is_correct_email("jjksajksd"))
print(is_correct_email("kubat@kubat.kg"))
print(is_correct_email("kubatkubat.kg"))