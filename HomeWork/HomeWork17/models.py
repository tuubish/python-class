class Shape:
    def draw(self, turtle):
        pass



class Rectangle(Shape):
    def __init__(self, width, height, color):
        self.width = width
        self.height = height
        self.color = color

    def draw(self, turtle):
        turtle.fillcolor(self.color)
        turtle.begin_fill()

        turtle.forward(self.width)
        turtle.left(90)

        turtle.forward(self.height)
        turtle.left(90)

        turtle.forward(self.width)
        turtle.left(90)

        turtle.forward(self.height)
        turtle.left(90)

        turtle.end_fill()
        turtle.forward(self.width)

class Circle(Shape):
    def __init__(self, radius, color):
        self.radius = radius
        self.color = color

    def draw(self, turtle):
        turtle.forward(self.radius)
        turtle.fillcolor(self.color)
        turtle.begin_fill()
        turtle.circle(self.radius)
        turtle.end_fill()
        turtle.forward(self.radius)

class Triangle(Shape):
    pass

class Line(Shape):
    pass

class Cross(Shape):
    pass

class Star(Shape):
    pass

class Ellipse(Shape):
    pass

class Tree(Shape):
    pass