import tkinter as tk
from tkinter import messagebox

calc = tk.Tk()


calc.title("Простой калькулятор")
calc.geometry("300x300+400+100")

sum_value = tk.StringVar(calc, "Результат")

sum = tk.Label(calc, textvariable=sum_value, bg="red", font="arial", fg="#fff")
sum.pack()



number1 = tk.Label(calc, text="Число1:")
number1.pack()
input1 = tk.Text(calc, width=30, height=1)
input1.pack()

number2 = tk.Label(calc, text="Число 2:")
number2.pack()
input2 = tk.Text(calc, width=30, height=1)
input2.pack()

def calc_sum():
    sum_of_numbers = int(input1.get(1.0, tk.END)) + int(input2.get(1.0, tk.END))
    sum_value.set("Сумма: " + str(sum_of_numbers))

sum_button = tk.Button(calc, text="Вычислить сумму", command=calc_sum)
sum_button.pack()



tk.mainloop()

