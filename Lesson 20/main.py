# Tkinter widgets - Registration form
import tkinter as tk
from tkinter import *

root = tk.Tk()
root.title("Become a Nike member")
root.geometry("500x550+200+100")

left_frame = tk.Frame(root)
left_frame.pack(fill=tk.BOTH, expand=1, side=tk.LEFT, padx=15, pady=5)

name_label = tk.Label(left_frame, anchor=tk.W, text="First Name")
name_label.pack()

name_input = tk.Entry(left_frame)
name_input.pack(fill=tk.X)

bd_frame = tk.Frame(left_frame)
bd_frame.pack(fill=tk.X)

birthday_label = tk.Label(bd_frame, anchor=tk.W, text="Date of birth:")
birthday_label.pack(fill=tk.X)

months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
month_var = tk.StringVar(bd_frame, value="---")

month_menu = tk.OptionMenu(bd_frame, month_var, *months)
month_menu.pack(anchor=tk.N, side=LEFT)

days = range(1, 32)
day_var = tk.StringVar(bd_frame, value="--")

day_menu = tk.OptionMenu(bd_frame, day_var, *days)
day_menu.pack(anchor=tk.N, side=LEFT)

years = range(1960, 2021)
year_var = tk.StringVar(bd_frame, value="---")

year_menu = tk.OptionMenu(bd_frame, year_var, *years)
year_menu.pack(anchor=tk.N, side=LEFT)


gender_frame = tk.Frame(left_frame)
gender_frame.pack(fill=tk.X)

gender_label = tk.Label(gender_frame, anchor=tk.W, text="Gender")
gender_label.pack(fill=tk.X, anchor=None)

gender_var = tk.IntVar()

male_gender_button = tk.Radiobutton(gender_frame, text="Male", variable=gender_var, value=1)
male_gender_button.pack(anchor=tk.N, side=LEFT)

woman_gender_button = tk.Radiobutton(gender_frame, text="Female", variable=gender_var, value=2)
woman_gender_button.pack(anchor=tk.N, side=LEFT)

subscription_var = tk.IntVar()

subscription_checkbox = tk.Checkbutton(left_frame, text="Subscribe to Newsletter", variable=subscription_var)
subscription_checkbox.pack(anchor=tk.N, pady=10)

btn_bg = tk.PhotoImage(file="button.png")
resize = btn_bg.subsample(1, 2)

submit_button = tk.Button(left_frame, image=btn_bg, text="Submit")
submit_button.pack(fill=tk.X)

right_frame = tk.Frame(root)
right_frame.pack(fill=tk.BOTH, expand=1, side=tk.RIGHT, padx=15, pady=5)

last_name_label = tk.Label(right_frame, anchor=tk.W, text="Last Name")
last_name_label.pack()

last_name_input = tk.Entry(right_frame)
last_name_input.pack(fill=tk.X)


root.mainloop()