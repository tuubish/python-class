# Создать функцию draw_rect() в файле rectangle.py
# которая рисует приямогульник
# и возвращает черепашку в исходное положение

import turtle

from rectangle import draw_rect
turtle.reset()

draw_rect(turtle, 10, 100, "blue")
turtle.forward(15)
draw_rect(turtle, 10, 100, "green")
turtle.forward(15)
draw_rect(turtle, 10, 100, "red")
turtle.forward(15)
draw_rect(turtle, 10, 100, "orange")

turtle.mainloop()