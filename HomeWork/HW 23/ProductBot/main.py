from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
import commands

# Link to bot: @KProductsBot

TOKEN = "1351726220:AAEYX-wjIE3yhod9ZqH8wGr9A7SdgHzkAAM"

updater = Updater(TOKEN, use_context=True)

dispatcher = updater.dispatcher

start_handler = CommandHandler("start", commands.on_start)
list_handler = CommandHandler("list", commands.product_list)
clear_handler = CommandHandler("emptylist", commands.clear_list)
conversation_handler = ConversationHandler(
    entry_points=[CommandHandler("additem", commands.add_item)],
    states={
        1: [MessageHandler(Filters.text, commands.item_name)],
        2: [MessageHandler(Filters.text, commands.item_description)],
        3: [MessageHandler(Filters.text, commands.item_price)],
    },
    fallbacks=[[CommandHandler("cancel", commands.cancel)]]
)


# Adding handlers
dispatcher.add_handler(start_handler)
dispatcher.add_handler(list_handler)
dispatcher.add_handler(conversation_handler)
dispatcher.add_handler(clear_handler)

print("Bot started!")
updater.start_polling()
updater.idle()

