from telegram.ext import ConversationHandler
import csv
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove

product = {
    "name": "",
    "description": "",
    "price": 0,
}


def on_start(update, context):
    update.message.reply_text("Привет! Выбери команду:\n"
                              "/list - Список продуктов\n"
                              "/additem - Добавь продукт в сисок\n"
                              "/emptylist - Очисти список, чтобы начать заново")


def product_list(update, context):
    try:
        update.message.reply_text("Название | Описание | Цена")
        with open("products.csv",  "r", encoding="utf-8") as show_list:
            reader = csv.reader(show_list)
            for row in reader:
                update.message.reply_text(row[0] + " | " + row[1] + " | " + row[2])
    except:
        update.message.reply_text("Список пуст. Чтобы добавить наберите команду: /additem")

def clear_list(update, context):
    open_file = open("products.csv", "w")
    open_file.close()
    update.message.reply_text("Список очищен")

def add_item(update, context):
    update.message.reply_text("Введите название")
    return 1

def item_name(update, context):
    update.message.reply_text("Введите описание продукта")
    product["name"] = update.message.text
    return 2

def item_description(update, context):
    product["description"] = update.message.text
    update.message.reply_text("Введите сумму проекта")
    return 3

def item_price(update, context):
    product["price"] = update.message.text
    with open("products.csv", "a", encoding="utf-8") as add_product:
        add_product.write(product["name"] + "," + product["description"] + "," + product["price"] + "\n")

    update.message.reply_text("Продукт добавлен")
    return ConversationHandler.END


def cancel(update, context):
    pass