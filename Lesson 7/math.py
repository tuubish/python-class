from math import pi, floor, ceil
from random import randint, choice, shuffle, random

# print(random()) # 0.0 - 1.0
# print(randint(-1000, 100)) # целые числа
# names = []
#
# print(choice(names)) # случайный выбор из списка
#
# shuffle(names) # перетусовка списка
# print(names)
#
# print(random() * 100)

numbers = randint(0, 5)

if numbers == int(input()):
    print("Cool!")
else:
    print("Try again!")