from unittest import TestCase
from functions.email import is_correct_email

class EmailFunctionTestCase(TestCase):
    def test_masteraalish_TrueCase(self):
        self.assertTrue(is_correct_email("masteraalish@gmail.com"))

    def test_themonrealstudio_TrueCase(self):
        self.assertTrue(is_correct_email("themonrealstudio@gmail.com"))

    def test_themonrea_FalseCase(self):
        self.assertFalse(is_correct_email("themonrea@lstudio@gmail.com"))

    def test_gmail_FalseCase(self):
        self.assertFalse(is_correct_email("@gmail.com"))

    def test_aidana_yahoo_FalseCase(self):
        self.assertFalse(is_correct_email("aidana.yahoo.com"))