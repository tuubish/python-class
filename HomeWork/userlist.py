from users import User, Group
from tablePrinter import TablePrinter

user1 = User()
user1.username = "kubat"
user1.email = "kubat.usubaliev@gmail.com"
user1.password = "qwertyui"

user2 = User()
user2.username = "alex"
user2.email = "alex@mail.ru"
user2.password = "qwertyui"

user3 = User()
user3.username = "white"
user3.email = "white@gmail.com"
user3.password = "qwertyui"

python_group = Group()
python_group.name = "Python-03"
python_group.users = [user1, user2, user3]

printer = TablePrinter()
printer.print(python_group)