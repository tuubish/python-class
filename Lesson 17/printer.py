class TablePrinter:
    def print(self, group):
        print("username | email | password")

        for user in group.users:
            mask = ""
            for i in range(len(user.password)):
                mask += "*"

            print(user.username, user.email, mask, sep= " | ")

    def print_only_gmail(self, group):
        print("username | email | password")

        for user in group.users:
            if user.email.endswith("@gmail.com"):
                mask = ""
                for i in range(len(user.password)):
                    mask += "*"

                print(user.username, user.email, mask, sep=" | ")