from tkinter import *

root = Tk()
root.title("New interface")
root.geometry("400x300+300+250")

btn = Button(text="Hello",
             background="#555",
             foreground="#ccc",
             padx="20",
             pady="8",
             font="16"
             )

btn2 = Button(root, text="Button 2")

btn.pack()
btn2.pack()

root.mainloop()