def gcd(number1, number2):
    while True:
        big = max(number1, number2)
        small = min(number1, number2)

        reminder = big % small
        if reminder == 0:
            return small
        else:
            number1 = reminder
            number2 = small
