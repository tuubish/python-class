from unittest import TestCase
from functions.gcd import gcd

class GcdFunctionTestCase(TestCase):
    def test_10_and_5_gcd_is_5(self):
        self.assertEqual(5, gcd(10, 5))

    def test_33_and_27_gcd_is_3(self):
        self.assertEqual(3, gcd(33, 27))

    def test_99_and_45_gcd_is_9(self):
        self.assertEqual(9, gcd(99, 45))

    def test_10_and_100_gcd_is_10(self):
        self.assertEqual(10, gcd(10, 100))

    def test_97_and_23_gcd_is_1(self):
        self.assertEqual(1, gcd(97, 23))


