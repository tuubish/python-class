import turtle


def draw_flower(height, size):
    height = 50
    flower_size = 23
    for i in range(5):
        turtle.up()
        turtle.forward(50)
        turtle.down()
        turtle.left(90)
        turtle.forward(height)
        turtle.right(90)
        turtle.begin_fill()
        turtle.circle(flower_size)
        turtle.end_fill()
        turtle.right(90)
        turtle.forward(height)
        turtle.left(90)

        flower_size -= 4
        height -= 10

turtle.reset()

turtle.up()
turtle.left(180)
turtle.forward(250)
turtle.left(180)
turtle.down()

turtle.fillcolor("red")

flowers()






turtle.mainloop()