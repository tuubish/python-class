from telegram.ext import ConversationHandler
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove

data = {
    "source": "",
    "sum": 0,
    "result": "",
}

def convert_currency(source_currency, sum, result_currency):
    rate = 34.3
    return rate * sum

def start_conversion(update, context):
    currencies = [
        ["EUR", "USD", "RUB"],
        ["TRY", "KGS", "UZS"]
    ]

    buttons = ReplyKeyboardMarkup(currencies)

    update.message.reply_text("Enter your currency", reply_murkup=buttons)
    return 1

def on_source_enter(update, context):
    data["source"] = update.message.text
    update.message.reply_text("Enter amount to convert", reply_markup=ReplyKeyboardRemove())
    return 2

def amount_enter(update, context):
    currencies = [
        ["EUR", "USD", "RUB"],
        ["TRY", "KGS", "UZS"]
    ]

    buttons = ReplyKeyboardMarkup(currencies)
    try:
        data["sum"] = float(update.message.text)
        update.message.reply_text("Enter result currency", reply_markup=buttons)
        return 3
    except:
        update.message.reply_text("Enter amount again")
        return 2

def on_result_currency(update, context):
    data["result"] = update.message.text

    result = convert_currency(data["source"], data["sum"], data["result"])
    result_currency = data["result"]

    update.message.reply_text("Result: %.2f %s" % (result, result_currency), reply_murkup=ReplyKeyboardRemove())
    return ConversationHandler.END

def cancel(update,context):
    update.message.reply_text("Bay! Bay!")
    ConversationHandler.END