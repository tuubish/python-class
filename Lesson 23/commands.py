import re

def on_start(update, context):
    chat = update.effective_chat
    context.bot.send_message(chat_id=chat.id, text="Hi! I'm your bot")

def on_convert(update, context):
    regex = r"Переведи \$(\d+) на (\w+)"
    match = re.match(regex, update.message.text)
    dollars = float(match.group(1))
    currency = match.group(2)

    rubles = dollars * 68

    message = "Получилось %.2f рублей" % rubles
    update.message.reply_text(message)