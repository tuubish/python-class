# Наследование

class Transport:
    def __init__(self):
        self.speed = 0

    def time_for(self, km):
        time = km / self.speed
        return time

class Car(Transport):
    def __init__(self, model):
        super().__init__()
        self.model = model

class ElectricBycicle(Transport):
    def __init__(self, destiny):
        super().__init__()
        self.destiny = destiny

    def battery_life(self, h):
        life = h / self.destiny
        return life
