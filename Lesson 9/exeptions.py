# Исключительные ситуации
# Exeptions

numbers = [23, 4.5, 3.14]
print(numbers[2])

try:
    print(numbers[4])
except:
    print("Элемента с таким индексом нет")

print(numbers[1])

# while True:
#     try:
#         index = int(input())
#         if index == -1:
#             break
#
#         try:
#             print(numbers[index])
#         except:
#             print("Элемента с индексом {} не существует".format(index))
#     except:
#         print("Введите целое число")


while True:
    try:
        index = int(input())
    except:
        print("Введите целое число")
        continue

    if index == -1:
        break

    try:
        print(numbers[index])
    except:
        print("Элемента с индексом {} не существует".format(index))
