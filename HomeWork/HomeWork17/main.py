from models import *
import turtle
from drawer import *


rect1 = Rectangle(40, 100, "blue")
rect2 = Rectangle(50, 50, "red")

circle1 = Circle(30, "green")

drawer = Drawer(turtle)

drawer.prepare()

drawer.draw_shape(rect1)
drawer.draw_shape(circle1)

drawer.finish()