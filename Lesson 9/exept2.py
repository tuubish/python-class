# Обработка конкретного типа ошибки

students = ["Arlan J", "Wiskey Fox", "Alisher"]
try:
    numb1 = int(input("First number"))
    numb2 = int(input("First number"))

    result = numb1 / numb2
    print(result)
except ValueError:
    print("Введите целое число")
except IndexError:
    print("Такой индекс отсутствует")
except:
    print("Что-то пошло не так, введите заново")