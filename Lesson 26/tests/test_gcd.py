from main import gcd

def assertEqual(expected, actual):
    if expected == actual:
        print("Test passed")
    else:
        raise AssertionError("Test failed: Expected {}, returned {}". format(expected, actual))

assertEqual(5, gcd(10, 5))
assertEqual(3, gcd(33, 27))
assertEqual(9, gcd(99, 45))
assertEqual(10, gcd(10, 100))
assertEqual(1, gcd(97, 23))