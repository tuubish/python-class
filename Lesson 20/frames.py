# Tkinter widgets - Registration form
import tkinter as tk
from tkinter import *

root = tk.Tk()
root.title("Become a Nike member")
root.geometry("500x550+200+100")

left_frame = tk.Frame(root, bg="blue")
left_frame.pack(fill=tk.BOTH, expand=1, side=tk.LEFT)

left_inner1_frame = tk.Frame(left_frame, height=100, bg="orange")
left_inner1_frame.pack(fill=tk.X, side=tk.BOTTOM)

right_frame = tk.Frame(root, bg="red")
right_frame.pack(fill=tk.BOTH, expand=1, side=tk.LEFT)

right_inner1_frame = tk.Frame(right_frame, height=100, bg="cyan")
right_inner1_frame.pack(fill=tk.X, side=tk.TOP)

root.mainloop()