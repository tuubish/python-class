# Рисование в tKinter
from tkinter import *

Key_Space = 32
key_up = 65362
key_down = 65364
key_left = 65361
key_right = 65363

width = 800
height = 800

root = Tk()
root.title("TKinter Game")
root.geometry("{}x{}+300+100".format(width, height))

canvas = Canvas(root, width=width, height=height)
canvas.pack()

cell_size = 40
rects = {}
player_x = 10
player_y = 20
field = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]



for x in range(20):
    for y in range(20):
        x1 = x * cell_size
        x2 = x1 + cell_size

        y1 = y * cell_size
        y2 = y1 + cell_size
        rect = canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill="#2d356f")
        key = "{}x{}".format(x, y)
        rects[key] = rect



def clear():
    item_color = "#ed356f"

    for x in range(20):
        for y in range(20):
            field_value = field[y][x]
            color = "#2d356f"
            if field_value == 1:
                color = "#ccc"

            key = "{}x{}".format(x, y)
            rect = rects[key]
            canvas.itemconfig(rect, fill=color)

score = 0

def breadcrumbs():
    global score
    field_value = field[player_y][player_x]
    if field_value == 1:
        field[player_y][player_x] = 0
        score += 1

def wall():
    pass

def on_keypress(event):
    global player_y, player_x

    key = event.keysym_num
    print(key)
    if key == key_up:
        player_y -= 1
    elif key == key_down:
        player_y += 1
    elif key == key_right:
        player_x += 1
    elif key == key_left:
        player_x -= 1

    breadcrumbs()

    clear()

    key = "{}x{}".format(player_x, player_y)
    rect = rects[key]
    canvas.itemconfig(rect, fill="orange")
clear()


root.bind("<Key>", on_keypress)


root.mainloop()