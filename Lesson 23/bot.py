from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
import commands
import conversion
import re

TOKEN = "1313817199:AAFO43knZcJNIPDJ7GXXunOm2l50tBcp95Q"

updater = Updater(TOKEN, use_context=True)

dispatcher = updater.dispatcher

start_handler = CommandHandler("start", commands.on_start)
dispatcher.add_handler(start_handler)

regex = r"Переведи \$(\d+) на рубли"
convert_handler = MessageHandler(Filters.regex(regex), commands.on_convert)
dispatcher.add_handler(convert_handler)

conversation_handler = ConversationHandler(
    entry_points=[CommandHandler("convert", conversion.start_conversion)],
    states={
        1: [MessageHandler(Filters.text, conversion.on_source_enter)],
        2: [MessageHandler(Filters.text, conversion.amount_enter)],
        3: [MessageHandler(Filters.text, conversion.on_result_currency)],
    },
    fallbacks=[CommandHandler("cancel", conversion.cancel)]
)

dispatcher.add_handler(conversation_handler)

print("Bot started")
updater.start_polling()
updater.idle()