def monthes(number):
    if number == "01":
        return "январь"
    elif number == "02":
        return "февраль"
    elif number == "03":
        return "март"
    elif number == "04":
        return "апрель"
    elif number == "05":
        return "май"
    elif number == "06":
        return "июнь"
    elif number == "07":
        return "июль"
    elif number == "08":
        return "август"
    elif number == "09":
        return "сентябрь"
    elif number == "10":
        return "октябрь"
    elif number == "11":
        return "ноябрь"
    elif number == "12":
        return "декабрь"