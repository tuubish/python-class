from telegram.ext import ConversationHandler
import csv
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove

cash_data = {
    "cash_for": "",
    "amount": 0
}

def starting(update, context):
    update.message.reply_text("Привет! Выбери команды ниже:\n"
                              "/list - Список всех расходов\n"
                              "/spent - Добавить куда были потрачены деньги\n"
                              "/cancel - Отменить добавление\n"
                              "/clear - Очистить список расходов")


def cash_list(update, context):
    update.message.reply_text("Назначение | Сумма")
    with open("cash.csv", "r", encoding="utf-8") as lists:
        reader = csv.reader(lists)
        for row in reader:
            update.message.reply_text(row[0] + " | " + row[1])
        # update.message.reply_text("Список пуст. Чтобы добавить наберите команду: /spent")

def add_spent(update, context):
    # choices = [
    #     ["Транспорт", "Еда вне дома", "Развлечение"],
    #     ["Покупки", "Продукты", "Кредиты"]
    # ]
    #
    # buttons = ReplyKeyboardMarkup(choices)
    update.message.reply_text("Выберите вид расхода")
    return 1

def spent_for(update, context):
    cash_data["cash_for"] = update.message.text
    update.message.reply_text("Введите сумму расхода")
    return 2

def spent_amount(update, context):
    cash_data["amount"] = update.message.text
    with open("cash.csv", "a", encoding="utf-8") as write_down:
        write_down.write(cash_data["cash_for"] + "," + cash_data["amount"] + "\n")
    update.message.reply_text("Расход записан")
    return ConversationHandler.END

def clear(update, context):
    open_file = open("cash.csv", "w")
    open_file.close()
    update.message.reply_text("Список очищен")

def cancel(update, context):
    pass