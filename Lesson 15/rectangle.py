
def draw_rect(turtle, width, height, color):
    turtle.fillcolor(color)
    turtle.begin_fill()

    turtle.forward(width)
    turtle.left(90)

    turtle.forward(height)
    turtle.left(90)

    turtle.forward(width)
    turtle.left(90)

    turtle.forward(height)
    turtle.left(90)

    turtle.end_fill()
