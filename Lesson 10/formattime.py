# Форматирование даты
from datetime import datetime, date, time

date1 = datetime.now()

datestr = date1.strftime("%Y.%m.%d %H:%M")
datestr1 = date1.strftime("%d.%m.%Y %H:%M:%S")
datestr2 = date1.strftime("%d.%m.%Y %H:%M")
print(datestr)
print(datestr1)
print(datestr2)

print(date1.year)
print(date1.month)
print(date1.hour)
print(date1.minute)
print(date1.second)
print(date1.microsecond)