# https://www.geeksforgeeks.org/sorting-algorithms/
# Цикл от 0 до -1



# 2. Сортировка Бока (God sort)
#
# Пока список не сортирован:
#   случайным образом перемешивать список
#
from random import shuffle

# def is_sorted(values) -> bool:
#     for i in range(len(values) - 1):
#         if values[i] > values[i+1]:
#             return False
#     return True
#
# def god_sort(values: list):
#     while not is_sorted(values):
#         shuffle(values)
#
# list = [4, 5, 1, 2]
# god_sort(list)
# print(list)

def bubble_sort(values: list):
    for i in range(len(values) - 1):
        for j in range(i+1, len(values)):
            if values[j] < values[i]:
                values[j], values[i] = values[i], values[j]

