# TDD - Test Drive Development
def plus(a, b):
    return a + b


def gcd(number1, number2):
    while True:
        big = max(number1, number2)
        small = min(number1, number2)

        reminder = big % small
        if reminder == 0:
            return small
        else:
            number1 = reminder
            number2 = small


def bubble_sort(values: list):
    # return
    for i in range(len(values) - 1):
        for j in range(i+1, len(values)):
            if values[j] < values[i]:
                values[j], values[i] = values[i], values[j]


