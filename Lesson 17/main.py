from transport import Transport, Car, ElectricBycicle

honda_fit = Transport()
honda_fit.speed = 50

hours = honda_fit.time_for(100)
print(hours)

mercedes_benz = Car("Mercedes Benz 221")
mercedes_benz.speed = 120

print(mercedes_benz.time_for(300))

eb = ElectricBycicle(120)

print(eb.battery_life(100))