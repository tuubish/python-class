from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
import commands

token = "1398818358:AAEHNo8uw-nDT13OHGhr7olWxucYF7UIr2g"
# Link to bot: @kubatcashbot

updater = Updater(token, use_context=True)
dispatcher = updater.dispatcher

start_handler = CommandHandler("start", commands.starting)
list_handler = CommandHandler("list", commands.cash_list)
clearing_handler = CommandHandler("clear", commands.clear)
conversation_handler = ConversationHandler(
    entry_points=[CommandHandler("spent", commands.add_spent)],
    states={
        1: [MessageHandler(Filters.text, commands.spent_for)],
        2: [MessageHandler(Filters.text, commands.spent_amount)],
    },
    fallbacks=[[CommandHandler("cancel", commands.cancel)]]
)



# Adding handler
dispatcher.add_handler(start_handler)
dispatcher.add_handler(list_handler)
dispatcher.add_handler(clearing_handler)
dispatcher.add_handler(conversation_handler)

print("Bot started")
updater.start_polling()
updater.idle()