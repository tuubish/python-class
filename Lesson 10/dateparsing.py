#Чтение даты или перевод строки в дату

from datetime import datetime

star_date1 = "17.06.2020 09:00"

parsed_date1 = datetime.strptime(star_date1, "%d.%m.%Y %H:%M")

print(parsed_date1)

star_date2 = "2020/30/01"

print(datetime.strptime(star_date2, "%Y/%d/%m"))
print(datetime.strptime(star_date2, "%Y/%d/%m").date())

str_time1 = "09:00"

time1 = datetime.strptime(str_time1, "%H:%M")
print(time1)

input_date = input("Введите дату в формате дд.мм.гггг: ")
date3 = datetime.strptime(input_date, "%d.%m.%Y")
print(date3)