class Calculator:
    def calc(self):
        return 0

class CreditCalculator(Calculator):
    def __init__(self, years, rate, sum):
        self.years = years
        self.rate = rate
        self.sum = sum

    def calc(self):
        return self.years * self.rate * self.sum

    def get_rate(self):
        return self.rate / 100


class AnnuitetCredit(CreditCalculator):
    def __init__(self, years, rate, sum, monthly_rate):
        super().__init__(years, rate, sum)
        self.monthly_rate = monthly_rate

    def calc(self):
        return self.years * (self.rate / 100) * self.sum