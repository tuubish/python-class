import sqlite3

# Создание подключени к БД, файл создастся автоматически
conn = sqlite3.connect("db.sqlite3")

# Создание таблицы книг
conn.execute("CREATE TABLE IF NOT EXISTS BOOKS(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, year INTEGER)")
# Добавление новых записей
conn.execute("INSERT INTO BOOKS(name, year) values ('Atlant Struggles', 1980), ('Магия утра', 2020)")
conn.commit()

class Book:
    def __init__(self, id, name, year):
        self.id = id
        self.name = name
        self.year = year

    def __repr__(self):
        return f"{self.id}. {self.name} {self.year}"


cursor = conn.cursor()

# Множествнное добавление записей
# books = [("Clean code", 2000), ("Everest", 2019)]
# cursor.executemany("INSERT INTO books (name, year) VALUES (?,?)", books)

cursor.close()
conn.commit()

# Получение данных из БД
cursor = conn.cursor()
result = cursor.execute("SELECT * from BOOKS WHERE year>?", [2000])

# Превращаем данные из БД в объекты класса Book
books = []
for row in result:
    id = row[0]
    name = row[1]
    year = row[2]
    book = Book(id, name, year)
    books.append(book)
    print(book)

# Также эти 6 строк можно коротко так написать
# books = [Book(row[0], row[1], row[2]) for row in result]

cursor.close()

# Удаление записей с таблицы
conn.execute("DELETE FROM books where id>4")
conn.commit()


# Так может выглядеть класс, который будет прослойкой межу основной программой и обращениями в БД

class BooksDbAdapter:
    def get_all_books(self):
        pass

    def save_new_book(self):
        pass

    def delete_book_by_id(self, id):
        pass

    def get_book_by_id(self, id) -> Book:
        pass

