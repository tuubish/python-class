# int()
# input()
# print()
# variables

# name = input("Привет! Как тебя зовут? ")
# print("Привет", name, "Meder", "Alisher", sep=", ")

# #Entering first number
# print("Введите первое число:")
# x = int(input())
#
# #Entering second number
# print("Введите второе число:")
# y = int(input())
#
# # Multiply First and Second number
# summ = x + y
#
# print("Сумма:", summ)


# birth_year = int(input("Введите год вашего рождения: "))
# current_year = 2020
#
# age = current_year - birth_year
#
# print("Вам сейчас", age, "лет")

# print("Your age is", int(input("Enter current year: ")) - int(input("Enter your birth year: ")))

sex = str(input("Введите ваш пол (w или m): "))
age = int(input("Сколько вам лет? "))

if (age >= 55 and sex == "w") or (age >= 65 and sex == "m"):
    print("Вам пора на пенсию")
else:
    print("Вам ещё работать и работать!")