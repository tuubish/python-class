class Boiler(object):
    def __init__(self):
        self.temp = 0
        self.volume = 0

    def boil(self, temperature):
        for t in range(self.temp, temperature):
            print("Current: " + str(t))
            self.temp = t

boiler = Boiler()