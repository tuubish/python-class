#Task 2. With functions
import os

def show():
    with open("books.db", "r") as book_list:
        list = book_list.read()
        print(list)

def add():
    with open("books.db", "a") as append:
        book_name = input("Название книги: ")
        book_tom = input("Том книги: ")
        book_author = input("Автор книги: ")
        book_year = input("Год пебликации книги: ")

        append.write(book_name+";"+book_tom+";"+book_author+";"+book_year)
        append.write("\n")
    print("Книига успешно доьавлена!")

def select():
    i = int(input("Введите индекс книги: "))
    with open("books.db", "r") as selected:
        lines = selected.readlines()
        print(lines[i])
        # for line in lines:
        #     print(line[i], end="")


while True:
    command = input("Введите команду: ")
    if command == "exit":
        break
    elif command == "show":
        show()
    elif command == "add":
        add()
    elif command == "select":
        select()