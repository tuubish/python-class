# for & while loops

# Ex. 1
# for i in range(10):
#     print(i ** 2)

# Ex. 2
# text = "Lorem ipsum"
#
# n = len(text)
#
# for i in range(n):
#     print(text[i])

# Ex. 3
# for i in range(10, 20, 2):
#     print(i)

# Ex. 4
# for i in range(3, 100, 3):
#     if i % 5 == 0:
#         print(i)
#
# for i in range(3, 100):
#     if i % 5 == 0 and i % 3 == 3:
#         print(i)

# Ex. 5 Lists

# list can contain
# age = [28, 30, 29]
# names = ["Alisher", "Nikita", "Adilet"]
#
# for split in range(len(names)):
#     name = names[split]
#     ages = age[split]
#
#     print(name + ",", ages)

# Ex. 6
# friends = []
#
# for i in range(3):
#     name = input("Enter names: ")
#     friends.append(name)
# print(friends)

# Ex. 7
# friends = []
# name = ""
# exit = "exit"
#
# while name != exit:
#     name = input("Enter name: ")
#     if name != exit:
#         friends.append(name)
# print(friends)

# Ex. 8
# friends = ["One", "Two", "Three"]
# print(friends)
#
# friends[1] = "Five"
# print(friends)
#
# del friends[2]
# print(friends)
#
# friends.insert(1, "Arestitel")
# print(friends)

# Ex. 9
cities = []
commands = ''
show = "show"
add = "add"
delete = "delete"
print("Введите необходимую команду:\n"
      "show - Показать список городов\n"
      "add - Добавить город\n"
      "del - Удалить город\n"
      "exit - Для выхода из программы")

while commands != "exit":
    commands = input()
    if commands == show:
        print(cities)
    elif commands == add:
        print("Введите название города для добавления: ")
        city_name = input()
        cities.append(city_name)
        print("Город добавлен")
    elif commands == delete:
        print("Введите индекс города:")
        index = int(input())
        del cities[index]