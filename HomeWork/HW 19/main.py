import tkinter as tk
from tkinter import *
from tkinter import messagebox
from datetime import datetime
import locale

locale.setlocale(locale.LC_ALL, "en")

now = datetime.now()


def date_calculation():
    try:
        correct_date = datetime.strptime(date_enter.get(), "%d/%m/%Y")
        year_var.set(datetime.strftime(correct_date, "%Y"))
        month_var.set(datetime.strftime(correct_date, "%B"))
        day_var.set(datetime.strftime(correct_date, "%d"))
        period = now - correct_date
        all_days_var.set(period.days)

    except:
        messagebox.showinfo("Format error", "Please, enter correct date!")


root = tk.Tk()

# Получение размеров экрана
screen_h = root.winfo_screenheight()
screen_w = root.winfo_screenwidth()

# Здесь мы создали переменные, где будем указывать размеры нашего окна
width = 400
height = 300

# Вычисляем отступы слева и сверху, чтобы наше окно было посередине экрана
offset_x = screen_w / 2 - width / 2
offset_y = screen_h / 2 - height / 2

# Название окна
root.title("Calendar")

# Размер окна и выводим его по центру
root.geometry("{}x{}+{}+{}".format(width, height, int(offset_x), int(offset_y)))

date_enter = StringVar()

date_label = Label(text="Enter date:")
date_label.grid(row=0, column=0, sticky="w")

date_input = Entry(textvariable=date_enter)
date_input.grid(row=0, column=1, padx=5, pady=5)

year_label = Label(text="Year:")
month_label = Label(text="Month:")
day_label = Label(text="Day:")
all_days_label = Label(text="All days:")

year_label.grid(row=1, column=0, sticky="e")
month_label.grid(row=2, column=0, sticky="e")
day_label.grid(row=3, column=0, sticky="e")
all_days_label.grid(row=4, column=0, sticky="e")

year_var = StringVar(value="Year")
month_var = StringVar(value="Month")
day_var = StringVar(value="Day")
all_days_var = StringVar(value="All days")

year = Label(textvariable=year_var)
month = Label(textvariable=month_var)
day = Label(textvariable=day_var)
all_days = Label(textvariable=all_days_var)

year.grid(row=1, column=1, sticky="w")
month.grid(row=2, column=1, sticky="w")
day.grid(row=3, column=1, sticky="w")
all_days.grid(row=4, column=1, sticky="w")


date_button = Button(text="Submit", command=date_calculation)
date_button.grid(row=5, column=0, sticky="e")

root.mainloop()