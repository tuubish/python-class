from users import User, Group
from printer import TablePrinter

user1 = User("kubat", "kubat.usubaliev@gmail.com", "qwertyu")
user2 = User("alisher", "alisher@kubat.kg", "123456789")
user3 = User("nail", "nail@gmail.com", "qazwsx")
user4 = User("alex", "alex@kubat.kg", "qwertgvcxs")
user5 = User("text", "qwert@kubat.kg", "hgfcvbnmjuyt")

python_group = Group()
python_group.name = "Python 03"
python_group.users = [user1, user2, user3, user4, user5]

printer = TablePrinter()
printer.print(python_group)
print()
printer.print_only_gmail(python_group)
