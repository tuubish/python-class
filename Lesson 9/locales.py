import locale
from datetime import datetime, date

locale.setlocale(locale.LC_ALL, "en")

now = datetime.now()
date1 = datetime(1990, 10, 2)

print(now.strftime("%d %b %Y (%A)"))
print(date1.strftime("%d %b %Y (%a)"))
print(date())